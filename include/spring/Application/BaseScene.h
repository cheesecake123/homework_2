#pragma once
#include <spring\Framework\IScene.h>
#include <qgridlayout.h>
#include <qspinbox.h>
#include <qpushbutton.h>
#include <qlabel>
#include <qlineedit.h>
#include <qobject.h>

namespace Spring
{
	class BaseScene : public IScene
	{

	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();

	private:
		void createGUI();

		QWidget *centralWidget;
		QGridLayout *gridLayout;
		QSpacerItem *verticalSpacer_2;
		QSpacerItem *verticalSpacer;
		QSpacerItem *horizontalSpacer;
		QSpacerItem *horizontalSpacer_2;
		QPushButton *backButton;
		QLabel *nameLabel;

		private slots:
		void mf_backButton();
		
	};

}
