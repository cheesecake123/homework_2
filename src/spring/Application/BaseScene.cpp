#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		createGUI();
		std::string name = boost::any_cast<std::string>(m_TransientDataCollection["Name"]);

		nameLabel->setText(QStringLiteral("Hello ").append(name.c_str()));

		QObject::connect(backButton, SIGNAL(released()), this, SLOT(mf_backButton()));
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}
	
	BaseScene::~BaseScene()
	{
	}

	void BaseScene::createGUI()
	{
		if (m_uMainWindow->objectName().isEmpty())
			m_uMainWindow->setObjectName(QStringLiteral("MainWindow"));
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_2, 3, 1, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 0, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("backButton"));
		backButton->setText("Go back");

		gridLayout->addWidget(backButton, 2, 1, 1, 1);

		nameLabel = new QLabel(centralWidget);
		nameLabel->setObjectName(QStringLiteral("nameLabel"));
		nameLabel->setAlignment(Qt::AlignCenter);

		gridLayout->addWidget(nameLabel, 1, 1, 1, 1);

		m_uMainWindow->setCentralWidget(centralWidget);
	}

	void BaseScene::mf_backButton()
	{
		const std::string c_szNextSceneName = "InitialScene";
		emit SceneChange(c_szNextSceneName);
	}
}
